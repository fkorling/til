# Today I Learned

My Today I Learned snippets. Inspired by [jbranchaud/til](https://github.com/jbranchaud/til), which I spotted [on Hacker News](https://news.ycombinator.com/item?id=22908044).

<!-- index starts -->
## python

* [Convert a datetime object to UTC without using pytz](https://gitlab.com/fkorling/til/blob/master/python/convert-to-utc-without-pytz.md) - 2020-04-19
* [macOS Catalina sort-of includes Python 3](https://gitlab.com/fkorling/til/blob/master/python/macos-catalina-sort-of-ships-with-python3.md) - 2020-04-21

## sqlite

* [Lag window function in SQLite](https://gitlab.com/fkorling/til/blob/master/sqlite/lag-window-function.md) - 2020-04-19
* [Null case comparisons in SQLite](https://gitlab.com/fkorling/til/blob/master/sqlite/null-case.md) - 2020-04-21

## cloudrun

* [Use labels on Cloud Run services for a billing breakdown](https://gitlab.com/fkorling/til/blob/master/cloudrun/use-labels-for-billing-breakdown.md) - 2020-04-21

## git

* [Useful git aliases](https://gitlab.com/fkorling/til/blob/master/git/useful-aliases.md) - 2020-04-24
* [Copy a version from one git branch to another?](https://gitlab.com/fkorling/til/blob/master/git/copy-versions-from-other-branch.md) - 2020-05-15

## gitlab

* [Gitlab CI Linting](https://gitlab.com/fkorling/til/blob/master/gitlab/ci-linting.md) - 2020-04-25

## aws

* [Disabling typescript compiler error to allow deploytime evaluation](https://gitlab.com/fkorling/til/blob/master/aws/cdk-disable-type-checking.md) - 2020-05-15

## cmake

* [CMake Basics](https://gitlab.com/fkorling/til/blob/master/cmake/basics.md) - 2020-05-25

## arch

* [Clean Architecture and Domain](https://gitlab.com/fkorling/til/blob/master/arch/clean-architecture.md) - 2020-07-19
* [CQRS Architectures](https://gitlab.com/fkorling/til/blob/master/arch/cqrs-architectures.md) - 2020-07-19

## java

* [Java Class Path Globbing in JDK](https://gitlab.com/fkorling/til/blob/master/java/java-classpath-globbing.md) - 2020-10-29
<!-- index ends -->
