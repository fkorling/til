# Clean Architecture and Domain 

Starting blog entry: https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html

* 3 main variants over time (with addtional stuff)
   * Hexagonal
   * Onion
   * Clean architecture 

## Principles

![CleanArchitecture.jpg](CleanArchitecture.jpg)


* All dependencies point inwards to the domain model, *entities* capture business wide logic
* Inversion of control, used to for wiring
* Application layer contains all the business logic, centered around use cases
* Domain model models business entities, organized around aggregate roots
* Cross-cutting concerns reside in separate layer/package
* Database, API, 3rd party, OS and other specifics reside in their specific packages and depend on the domain model directly, or indirectly through the application layer. In practice, the domain model might depend on e.g., database layer for ORM implementation

## Consequences 

* Interfaces for dependent services are stored in the application layer
* In theory, there should be no implementation details (e.g., database artifacts) in the application layer, which requires additional abstraction

## TODO

* CommandHandler CQRS pattern

## More to read

* https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/
* https://herbertograca.com/2017/02/08/ddd-europe-2017/#domain-driven-organisation-design
