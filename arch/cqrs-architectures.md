# CQRS Architectures

* Domain centric design

Three main types of CQRS architectures: 

## 1. Single-database CQRS

* Single database
* Commands use domain to update state which is then saved to database
*  Queries executed directly against database


## 2. Two-database CQRS

* Two databases: Read db and Write DB
* Modification in write db are pushed to read db and might be out-of-sync, giving eventual consistency
* More complex but will handle high read loads better 

## 3. Event Sourcing CQRS

* Event store and read database
* Does not store state of entities directly, only events
* Replays events when need to use an event
* Pushes events to read store to update state
* Gives
   * complete audit trail
   * point-in-time construction
   * replay events for e.g., debug
   * multiple read databases
   * rebuild prodcution databases
* Write side can be optimized using snapshots



