# Disabling typescript compiler error to allow deploytime evaluation

Sometimes the type checking enforced by the typescript compiler will interfere with the CDK synthesis process.

A case is when an attribute from e.g., a *custom resource* needs to be evaluated **and** some other expression need to be evaluated at the same time, e.g., a type conversion.

Example of how this could fail: 

```typescript
const customResource = new cdk.CustomResource(...);
const v : number = parseInt(customResource.getAttString('attribute'));
```

With out the `parseInt` operator the compiler's type checking would fail the above line. However, when synthesizing the `parseInt` function call would be evaluated and return `null` as the `getAttString` would at this time not contain a valid integer. 

Instead, evaluation needs to be delayed until deploytime, which is how `getAttString` works but removing the `parseInt` will cause the compilation to fail.

The solution is to instruct the compiler to *ignore the type error* on the line using `@ts-ignore`, see [Type Checking JavaScript Files](https://www.typescriptlang.org/docs/handbook/type-checking-javascript-files.html)

```typescript
const customResource = new cdk.CustomResource(...);
// @ts-ignore: Ignore mismatching types to delay evaluation
const v : number = customResource.getAttString('attribute');
```
