# CMake Basics

## Builds

First command creates Makefile(s) in the build directory, 
pointed to by `build`. Second commands executes `make` in the
`build` directory.

	cmake -S . -B build
	cmake --build build

## Verbose build

	VERBOSE=1 cmake --build build

## Build Types

What type of output to generate is controlled by the `CMAKE_BUILD_TYPE`
variable, given at generation time for Makefiles, as these only
allow a single configuration.

Some valid values are:

* *Release*
* *Debug*
* *RelWithDebInfo* -- add debug info to *Release* build, but might change optimization level
* *MinSizeRel* 


	cmake -DCMAKE_BUILD_TYPE=Release -S . -B build

See also [How can I build multiple modes without switching ?][1]
for how to have multiple build types at the same time.

[1]:	https://gitlab.kitware.com/cmake/community/-/wikis/FAQ#how-can-i-build-multiple-modes-without-switching-