# Java Class Path Globbing in JDK

The java/javac (and perhaps other JDK tools) support *globbing* of class path in its command line options.

Example:

```
java -cp "foo/*" MainClass
```

would configure a class path will all found JAR-files in the `foo` directory.

Multiple levels of directories can be provided, but are not searched recursively.

```
java -cp "foo/*:foo/bar/*" MainClass
```

The `CLASSPATH` environment variable provides the same functionaliy.

**Note:** The argument must be quoted to prevent the shell from exanding.


See https://docs.oracle.com/javase/7/docs/technotes/tools/solaris/classpath.html for more details on JDK class path options.

This has been the case since at least JDK 7, see https://docs.oracle.com/javase/7/docs/technotes/tools/solaris/java.html
