# Copy a version from one git branch to another?

Run this from the branch where you want the file to end up:

```git
git checkout otherbranch myfile.txt
````

General formulas:

```git
git checkout <commit_hash> <relative_path_to_file_or_dir>
git checkout <remote_name>/<branch_name> <file_or_dir>
````

Some notes:

* Using the commit hash you can pull files from any commit
* This works for files and directories
* overwrites the file myfile.txt and mydir
* Wildcards don't work, but relative paths do
* Multiple paths can be specified
