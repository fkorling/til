# Useful git aliases

Aliases make git a much more pleasant experince. 
The following section can be inserted in the `$HOME/.gitconfig` file
for usage.

```
[core]
	autocrlf = true
	editor = vi

[rerere]
	enabled = true

[rebase]
	autosquash = true

[commit]
	template = C:/Users/fkxhgz/.gitmessage

[fetch]
	prune = true

[push]
	default = current

[alias]
# status
st = status

# Short alias for checout
co = checkout

# checkout master
coma = checkout master

# checkout dev
codev = checkout develop

# cop
cop = checkout -

# Branch info, including remotes
bi = !git --no-pager branch -avv

# Branch info, only local
bil = !git --no-pager branch -vv

# Nicer log history
treelog = log --graph --oneline --decorate --parents --all

# Nice log with tree and relative dates
lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative
nlg = "!git -P log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"

# Nice log with tree and relative dates from master
lgm = "!git -P log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative master.."

# Nice log showing also patches
lgp ="!git -P log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative -p"

# Nice log showing patches from master
lgmp = "!git -P log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative -p master.."

# Nice log showing also changed files
lgf = "!git -P log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --name-only" 

# Nice log showing changed files from master
lgmf = "!git -P log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --compact-summary master.."

# Oneline log
log1 = log --oneline

# List commits showing changed files
ll = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --numstat

# Show modified files in last commit:
dl = "!git ll -1"

# Show a diff last commit:
dlc = diff --cached HEAD^

# Show the history of a file, with diffs
filelog = log -u

# git grep
grep = grep -Ii

# Show the last tag
lasttag = describe --tags --abbrev=0

# Show commit when file was added
added = log --diff-filter=A --

# Show commit when file was changed
changed = log --diff-filter=M --

# show files
dnames = diff --name-only

# list commits
ls = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate

# list aliases
la = "!git config -l | grep alias | cut -c 7-"
```