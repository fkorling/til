# Gitlab CI Linting

On the *Pipelines* page of any Gitlab project, there is a button **CI Lint**
that will allow linting of a `.gitlab-ci.yaml` file.

It will detect:

- Lexicographical YAML syntax errors, but will stop checking other stuff once found
- Gitlab CI YAML errors, such as missing keywords or wrong types

A summary of discovered and parsed information for each job in the `gitlab-ci.yaml` file is also printed at the end, 
such as found stages and jobs, and the respective parameters at each stage.
